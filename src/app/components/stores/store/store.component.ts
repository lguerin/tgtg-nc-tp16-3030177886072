import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StoreModel } from '../../../models/store.model';

@Component({
  selector: 'nc-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent implements OnInit {

  store: StoreModel;

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.store = this.route.snapshot.data.store;
  }

  back(): void {
    this.router.navigate(['/']);
  }
}
