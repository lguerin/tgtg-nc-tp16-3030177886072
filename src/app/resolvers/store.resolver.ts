import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { StoreModel } from '../models/store.model';
import { TgtgService } from '../services/tgtg.service';

@Injectable({
  providedIn: 'root'
})
export class StoreResolver implements Resolve<StoreModel> {

  constructor(private tgtgService: TgtgService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<StoreModel> {
    const storeId = route.paramMap.get('id');
    return this.tgtgService.getStore(storeId);
  }
}
